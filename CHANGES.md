# Changes

##### 1.0.3

- FEATURE: Enhanced PRTG Logic for cronjob checker.

##### 1.0.2

- FEATURE: Fixed Composer versioning.

##### 1.0.1

- FEATURE: Fixed required Shopware version.
- FEATURE: Added Documentation.

##### 1.0.0

- FEATURE: Added Cronjob checking and added Email Checking.
- FEATURE: Basic Plugin Structure.
