# Memo Mailjet

### Installation

To install the plugin you can follow the steps described below.

1. Add access key to bitbucket repository.

This can be done in the repository by going to "Repository Settings" and the choose the menu option "Access keys".

2. Add the repository to `composer.json`

If the key `repositories` does not exists yet you will need to create it.
Take a look at the partial `composer.json` and add the missing parts.
```json
{
    "repositories": [
        {
            "type": "path",
            "url": "custom/plugins/*/packages/*",
            "options": {
                "symlink": true
            }
        },
        {
            "type": "path",
            "url": "custom/static-plugins/*",
            "options": {
                "symlink": true
            }
        },
        {
            "type": "vcs",
            "url": "git@bitbucket.org:memo_development/memomailjetsw6.git"
        }
    ]
}
```

After you adapted the `composer.json` file you can require the plugin using composer with the following command.

```bash
composer require memo/mailjet-sw6
```

Now the Plugin should be available in the Shopware Backend.

### Configuration

- Mailjet API-Key

Here you will need to set the Mailjet API Key of the customer.

- Mailjet Secret

Here you will need to set the Mailjet Secret Key of the customer.

- Contactlist ID
  
Here you will need to set the ID of the contact list of the customer.

- Sync all Customers

If you add this option all customer will be synced.
Only those that accepted Newsletter in Shopware Backend will be added to the given contact list.

| INFO: please create a language contact-property in mailjet before sync
| --- |

### Usage

To run the synchronisation per console you can run the following command.
```bash
bin/console memo:mailjet:sync
```

So that the customers will be synced regularly you will ned to set the following entry into the crontab by running `crontab -e`.

```bash
20 * * * * /.../php /.../bin/console memo:mailjet:sync
```

### Updating for a new Shopware Version

If you want to use the Plugin in a newer Shopware version the first thing you need to adapt is the `composer.json`.
Set your new version like in the example below:
```
"shopware/core": "6.3.* || 6.4.*",
```
Then you will need to verify, if it still works.
If there are problems you will need to fix those and try to not create breaking changes.

If there is no way to not have breaking changes you will need to create new branches.

For Example:
```
release/6.3
release/6.4
```

Then you will need to adapt the `composer.json` file so that the old Shopware Version will not load the new broken release:
```
"shopware/core": "6.4.*",
```

After that you will need to set the version in the `composer.json` to one matching the release Branch.

```
"version": "v6.4.10",
```

Then you will need to tag your new version in git matching the version in composer.
