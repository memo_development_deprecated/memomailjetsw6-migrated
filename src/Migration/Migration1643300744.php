<?php declare(strict_types=1);

namespace Memo\Mailjet\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1643300744 extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1643300744;
    }

    public function update(Connection $connection): void
    {
        $this->createMailjetSyncTable($connection);

    }

    public function updateDestructive(Connection $connection): void
    {
        $sql = <<<'SQL'
            DROP TABLE IF EXISTS `memo_mailjet_sync_log`;
            SQL;
        $connection->executeStatement($sql);
    }

    /**
     * @param Connection $connection
     * @throws \Doctrine\DBAL\Exception
     */
    private function createMailjetSyncTable(Connection $connection) {

        $sql = <<<SQL
            CREATE TABLE IF NOT EXISTS `memo_mailjet_sync_log`
            (
                `id` BINARY(16) NOT NULL,
                `id_mj_contact` INT(11) NOT NULL,
                `id_mj_list` INT(11) NOT NULL,
                `id_mj_listrecipient` INT(11) NOT NULL,
                `id_sw_recipient` BINARY(16) NULL,
                `status` TINYINT NOT NULL,
                `updated_at` DATETIME(3) NULL,
                `created_at` DATETIME(3),
                PRIMARY KEY (`id`)
            ) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci;
            SQL;

        $connection->executeStatement($sql);
    }
}
