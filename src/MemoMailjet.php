<?php declare(strict_types=1);

namespace Memo\Mailjet;

use Shopware\Core\Framework\Plugin;
use Shopware\Core\Framework\Plugin\Context\UninstallContext;
use Shopware\Core\Framework\Plugin\Context\InstallContext;

if (file_exists(dirname(__DIR__) . '/vendor/autoload.php')) {
    require_once dirname(__DIR__) . '/vendor/autoload.php';
}

class MemoMailjet extends Plugin
{
    /**
     * @param InstallContext $context
     */
    public function install(Plugin\Context\InstallContext $context): void
    {

    }

    /**
     * @param UninstallContext $context
     */
    public function uninstall(UninstallContext $context): void
    {
        if ($context->keepUserData()) {
            return;
        }
    }
}
