<?php declare(strict_types=1);

namespace Memo\Mailjet\Command;

use Memo\Importer\Services\ImporterService;
use Memo\Mailjet\Services\SyncService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MailjetSyncCommand extends Command
{
    protected static $defaultName = 'memo:mailjet:sync';

    /**
     * @var SyncService
     */
    protected $syncService;

    /**
     * CalcTurnoverCommand constructor.
     *
     * @param SyncService $syncService
     */
    public function __construct(SyncService $syncService)
    {
        $this->syncService = $syncService;

        parent::__construct();
    }

    /**
     * Configure Command.
     */
    protected function configure()
    {
        $this
            ->setDescription('Converts Import File to a Shopware Import File')
            ->setHelp('This Importer converts the given CSV File into a Shopware readable import file.');
    }

    /**
     * Execute Command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->io->title('Mailjet Sync powered by Media Motion AG');
        $failed = false;

        $result = $this->syncService->sync($this->io);

        $this->io->writeln('');

        foreach($result['errors'] as $error) {
            $this->io->error($error);
            $failed = true;
        }

        foreach($result['info'] as $info) {
            $this->io->info($info);
        }

        if ($failed) {
            return 1;
        }

        $this->io->writeln('Successfully synced Customers');

        return 0;
    }
}
