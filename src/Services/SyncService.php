<?php
/**
 * Implemented by Media Motion AG team https://www.mediamotion.ch
 *
 * @copyright Media Motion AG https://www.mediamotion.ch
 * @license LGPL-3.0+
 * @link https://www.mediamotion.ch
 */

namespace Memo\Mailjet\Services;

use Mailjet\Client;
use Mailjet\Resources;
use Shopware\Core\Checkout\Customer\CustomerCollection;
use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Content\Newsletter\Aggregate\NewsletterRecipient\NewsletterRecipientEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Symfony\Component\Console\Style\SymfonyStyle;
use Memo\Mailjet\Custom\MemoMailjetSyncLogEntity;

/**
 * Class SyncService
 *
 * @package Memo\Mailjet\Services
 */
class SyncService
{
    /**
     * @var Context
     */
    private $context;

    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * @var MailjetSyncLogRepository
     */
    private $mailjetSyncRepository;

    /**
     * @var NewsletterRecipientEntity
     */
    private $newsletterRepository;


    /**
     * @var string|null
     */
    private $apiKey;

    /**
     * @var string|null
     */
    private $apiSecret;

    /**
     * @var string|null
     */
    private $contactListId;

    /**
     * @var bool
     */
    private $syncAllCustomers;

    /**
     * @var Client
     */
    private $client;

    /**
     * @param SystemConfigService $systemConfigService
     * @param EntityRepositoryInterface $customerRepository
     * @param EntityRepositoryInterface $mailjetSyncRepository
     * @param EntityRepositoryInterface $newsletterRepository
     */
    public function __construct(SystemConfigService $systemConfigService, EntityRepositoryInterface $customerRepository,EntityRepositoryInterface $mailjetSyncRepository, EntityRepositoryInterface $newsletterRepository)
    {
        $this->context = Context::createDefaultContext();
        $this->customerRepository = $customerRepository;
        $this->newsletterRepository = $newsletterRepository;
        $this->mailjetSyncRepository = $mailjetSyncRepository;
        $this->apiKey = $systemConfigService->get('MemoMailjet.config.apiKey');
        $this->apiSecret = $systemConfigService->get('MemoMailjet.config.apiSecret');
        $this->contactListId = $systemConfigService->get('MemoMailjet.config.contactListId');
        $this->syncAllCustomers = $systemConfigService->get('MemoMailjet.config.syncAllCustomers');
    }

    /**
     * Run the Mailjet Sync.
     *
     * @param SymfonyStyle $io
     */
    public function sync($io = null)
    {
        $result = [
            'info' => [],
            'errors' => []
        ];

        if (is_null($this->apiKey) || is_null($this->apiSecret) || is_null($this->contactListId)) {
            $result['errors'][] = 'Plugin configuration is missing.';

            return $result;
        }

        $criteria = (new Criteria())->addAssociations(['language', 'language.locale', 'salutation']);

        /** @var CustomerCollection $customers */
        $customers = $this->newsletterRepository->search($criteria, $this->context)->getEntities();

        if ($customers->count() === 0) {
            $result['info'][] = 'No customers matching criteria.';
            return $result;
        }

        $this->client = new Client($this->apiKey, $this->apiSecret, true, ['version' => 'v3', 'connect_timeout' => 20]);

        if (!$this->client) {
            $result['errors'][] = 'Failed to create Mailjet client.';

            return $result;
        }

        $progressBar = null;

        if (!is_null($io)) {
            $io->writeln('Starting pushing of customers.');
            $progressBar = $io->createProgressBar($customers->count());
            $progressBar->start();
        }

        /** @var  $customer */
        foreach ($customers as $customer) {
            $result['errors'] = array_merge($result['errors'], $this->pushCustomer($customer));

            if (!is_null($progressBar)) {
                $progressBar->advance();
            }

            if (count($result['errors']) > 0) {
                if (!is_null($progressBar)) {
                    $progressBar->advance();
                }

                return $result;
            }
        }

        if (!is_null($progressBar)) {
            $progressBar->finish();
        }

        return $result;
    }

    /**
     * @param CustomerEntity $customer
     */
    private function pushCustomer(NewsletterRecipientEntity $customer)
    {
        $errors   = [];
        $blnRecipient = false;
        $blnNoChanges = false;
        $blnNewMjStatus = false;
        $prmMjSyncId    = null;

        //Check if customer status ready 2 get Newsletters
        if (in_array($customer->getStatus(),['direct','optIn'])) {
            $blnRecipient = true;
            $blnNewMjStatus = true;
        }
        $response = $this->client->get(Resources::$Contact, ['id' => utf8_encode($customer->getEmail())]);
        $responseData = $response->getData();
        $mjContact = $responseData[0];
        $mjContactID = $responseData[0]['ID'];

        // Create Customer if not found.
        if (array_key_exists('StatusCode', $responseData) && $responseData['StatusCode'] === 404) {
            if(!$blnRecipient and !$this->syncAllCustomers) {
                //Customer not ready 2 get News (wrong status)
                return $errors;
            }
            $newCustomer = [
                'Email' => utf8_encode($customer->getEmail()),
            ];

            $response = $this->client->post(Resources::$Contact, ['body' => $newCustomer]);
            $responseData = $response->getData();

            if (!$response->success()) {
                $errors[] = $responseData['ErrorMessage'];
                return $errors;
            }

        } else {
            //Check Status from Mailjet and sync to sw
            $result   = NULL;
            $response = $this->client->get(Resources::$Listrecipient,['filters'=>['ContactsList'=>$this->contactListId,'ContactEmail' => utf8_encode($customer->getEmail())]]);
            if($response->getCount()) {
                $aMailJet = $response->getData()[0];
                $criteria = (new Criteria())->addFilter(new EqualsFilter('idMjListrecipient', $aMailJet['ID']));
                $result = $this->mailjetSyncRepository->search($criteria, $this->context)->getEntities();
                $aSyncLogData = current($result->getElements());
                $prmMjSyncId  = key($result->getElements());

            } else {

                //Auf keiner Kontaktliste vorhanden
                $prmAction = ($blnRecipient === true) ? 'addforce' : 'unsub';
                $aUpdateMjContact = [
                    'ContactsLists' => [
                        [
                            'ListID' => $this->contactListId,
                            'Action' => $prmAction
                        ]
                    ]
                ];

                $responseContactList     = $this->client->post(Resources::$ContactManagecontactslists, ['id' => $mjContactID, 'body' => $aUpdateMjContact]);
                $result = NULL;

                if (!$responseContactList->success()) {
                    $errors[] = $result['ErrorMessage'];
                }
            }

            if(is_null($result) or empty($result->getElements())) {
                $response = $this->client->get(Resources::$Listrecipient,['filters'=>['ContactsList'=>$this->contactListId,'ContactEmail' => utf8_encode($customer->getEmail())]]);

                if($response->getCount()) {
                    $aMailJet = $response->getData()[0];

                    //Create Log entry with current Status
                    $logID = Uuid::randomHex();

                    $newEntry = $this->mailjetSyncRepository->create([
                        [
                            'id' => $logID,
                            'idMjContact' => $aMailJet['ContactID'],
                            'idMjListrecipient' => $aMailJet['ID'],
                            'idMjList' => intval($this->contactListId),
                            'status' => $blnRecipient
                        ]
                    ], $this->context);
                    $prmMjSyncId = $logID;
                }

            }else{

                $lastStatus   = boolval($aSyncLogData->status);
                $currSwStatus = $blnRecipient;
                $currMjStatus = ($aMailJet['IsUnsubscribed'] === true) ? false : true;

                if($lastStatus !== $currMjStatus) {
                    //Changes in MJ
                    $aUpdate = ['id'=>$customer->getId()];
                    $aUpdate['status'] = $currMjStatus === true ? 'direct' : 'optOut';

                    //Update SW Newsletter DB
                    $this->newsletterRepository->update([$aUpdate],$this->context);

                    //Update SW Customer DB
                    $criteria = (new Criteria())->addFilter(new EqualsFilter('email', $customer->email),new EqualsFilter('salesChannelId', $customer->salesChannelId));
                    $customerResult = current($this->customerRepository->search($criteria, $this->context)->getEntities()->getElements());
                    if($customerResult !== false) {
                        //update if is Shop customer
                        $this->customerRepository->update([[
                            'id' => $customerResult->getId(),
                            'newsletter' => $currMjStatus
                        ]], $this->context);
                    }

                    //Update Log DB
                    $blnRecipient = $newMjStatus = boolval($currMjStatus);

                }elseif($lastStatus !== $currSwStatus) {
                    $newMjStatus = boolval($currSwStatus);

                }else{
                    $blnNoChanges = true;
                }
            }
        }


        //Update Mailjet Recipient Data
        $updateCustomer = [
            'Data' => []
        ];

        if (explode('-', $customer->getLanguage()->getLocale()->getCode())[0]) {
            $updateCustomer['Data'][] = [
                'Name' => 'language',
                'value' => explode('-', $customer->getLanguage()->getLocale()->getCode())[0],
            ];
        }

        if (utf8_encode(ucfirst($customer->getFirstName()))) {
            $updateCustomer['Data'][] = [
                'Name' => 'firstname',
                'value' => utf8_encode(ucfirst($customer->getFirstName()))
            ];
        }

        if (utf8_encode(ucfirst($customer->getLastName()))) {
            $updateCustomer['Data'][] = [
                'Name' => 'lastname',
                'value' => utf8_encode(ucfirst($customer->getLastName()))
            ];
        }

        if (!empty($customer->getSalutation()) and $customer->getSalutation()->getSalutationKey()) {
            $updateCustomer['Data'][] = [
                'Name' => 'salutation',
                'value' => $customer->getSalutation()->getSalutationKey()
            ];
        }

        if (utf8_encode($customer->getStreet())) {
            $updateCustomer['Data'][] = [
                'Name' => 'address',
                'value' => utf8_encode($customer->getStreet())
            ];
        }

        if (utf8_encode($customer->getCity())) {
            $updateCustomer['Data'][] = [
                'Name' => 'city',
                'value' => utf8_encode($customer->getCity())
            ];
        }

        if (utf8_encode($customer->getZipcode())) {
            $updateCustomer['Data'][] = [
                'Name' => 'plz',
                'value' => utf8_encode($customer->getZipcode())
            ];
        }

        $response = $this->client->put(Resources::$Contactdata, ['id' => $mjContactID, 'body' => $updateCustomer]);
        $responseData = $response->getData();

        if (!$response->success()) {
            $errors[] = $responseData['ErrorMessage'];

            return $errors;
        }

        //No Changes on Status return false;
        if($blnNoChanges === true) {
            //Update Log DB
            $this->mailjetSyncRepository->update([['id' => $prmMjSyncId, 'status' => $blnNewMjStatus]],$this->context);
            return $errors;
        }

        //Update Status on Mailjet
        if ($blnRecipient) {
            $contactList = [
                'ContactsLists' => [
                    [
                        'ListID' => $this->contactListId,
                        'Action' => 'addforce',
                    ]
                ]
            ];

        } else {
            $contactList = [
                'ContactsLists' => [
                    [
                        'ListID' => $this->contactListId,
                        'Action' => 'unsub',
                    ]
                ]
            ];
        }

        $response = $this->client->post(Resources::$ContactManagecontactslists, ['id' => $mjContactID, 'body' => $contactList]);
        $responseData = $response->getData();

        if (!$response->success()) {
            $errors[] = $responseData['ErrorMessage'];
        }

        //Update Log DB
        if(null !== $prmMjSyncId) {
            $this->mailjetSyncRepository->update([['id' => $prmMjSyncId, 'status' => $blnNewMjStatus]],$this->context);
        }
        return $errors;
    }
}
