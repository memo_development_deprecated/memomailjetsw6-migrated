<?php declare(strict_types=1);

namespace Memo\Mailjet\Custom;

use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\BoolField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IntField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Field\DateTimeField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;

class MemoMailjetSyncLogEntityDefinition extends EntityDefinition
{

    public const ENTITY_NAME = 'memo_mailjet_sync_log';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    /**
     * @return string
     */
    public function getEntityClass(): string
    {
        return MemoMailjetSyncLogEntity::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new PrimaryKey(), new Required()),
            (new IntField('id_mj_contact', 'idMjContact')),
            (new IntField('id_mj_listrecipient', 'idMjListrecipient')),
            (new IntField('id_mj_list', 'idMjList')),
            (new StringField('id_sw_recipient', 'idSwRecipient')),
            (new BoolField('status', 'status'))->addFlags(new Required()),
            new DateTimeField('updated_at', 'updatedAt'),
            new DateTimeField('created_at', 'createdAt'),
        ]);
    }
}
