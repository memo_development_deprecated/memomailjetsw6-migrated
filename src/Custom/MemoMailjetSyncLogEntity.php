<?php declare(strict_types=1);

namespace Memo\Mailjet\Custom;

use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;
use \DateTimeInterface;


class MemoMailjetSyncLogEntity extends Entity
{
    use EntityIdTrait;

    /**
     * @var int | null
     */
    protected $idMjContact;

    /**
     * @var int | null
     */
    protected $idMjList;

    /**
     * @var int | null
     */
    protected $idMjListrecipient;

    /**
     * @var string
     */
    protected $idSwRecipient;


    /**
     * @var bool
     */
    protected $status;

    /**
     * @var DateTimeInterface | null
     */
    protected $updatedAt;

    /**
     * @var DateTimeInterface | null
     */
    protected $createdAt;


    /**
     * @return int
     */
    public function getIdMjContact(): int {
        return $this->id_mjContact;
    }

    /**
     * @param string $idMjContact
     */
    public function setIdMjContact(int $idMjContact): void {
        $this->id_mjContact = $idMjContact;
    }

    /**
     * @return int
     */
    public function getIdMjList(): int {
        return $this->idMjList;
    }


    /**
     * @param string $idMjList
     */
    public function setIdMjList(int $idMjList): void {
        $this->idMjList = $idMjList;
    }

    /**
     * @return int
     */
    public function getIdMjListrecipient(): int {
        return $this->idMjListrecipient;
    }

    /**
     * @param string $idMjList
     */
    public function setIdMjListrecipient(int $idMjListrecipient): void {
        $this->idMjListrecipient = $idMjListrecipient;
    }

    /**
     * @return bool
     */
    public function getStatus() : bool {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status): void {
        $this->status = $status;
    }


    /**
     * @param DateTimeInterface|null $updatedAt
     */
    public function setUpdatedAt(?DateTimeInterface $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTimeInterface|null $createdAt
     */
    public function setCreatedAt(?DateTimeInterface $createdAt): void
    {
        $this->updatedAt = $createdAt;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

}
